package edu.ntnu.idatt2001.CardGame;

import edu.ntnu.idatt2001.CardGame.Controller.HandOfCardsController;
import edu.ntnu.idatt2001.CardGame.Model.HandOfCards;
import edu.ntnu.idatt2001.CardGame.View.HandOfCardsButtonView;
import edu.ntnu.idatt2001.CardGame.View.HandOfCardsStatsView;
import edu.ntnu.idatt2001.CardGame.View.HandOfCardsView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Class responsible for first page of application.
 *
 * @author vemundroe
 */
public class CardGameApplication extends Application {

  public static void main(String[] args) {
    launch();
  }
  @Override
  public void start(Stage stage) throws FileNotFoundException {
    Scene scene = new Scene(createAndConfigureBorderPane(), 960,  600);
    stage.setTitle("Card game");
    stage.getIcons().add(new Image(new FileInputStream("src/main/resources/images/pokerIcon.png")));
    stage.setScene(scene);
    stage.show();
  }

  /**
   * Method to create and configure the root border pane.
   *
   * @return the configured border pane
   */
  private BorderPane createAndConfigureBorderPane() {
    HandOfCards handOfCards = new HandOfCards(5);
    HandOfCardsController handOfCardsController = new HandOfCardsController(handOfCards);

    HandOfCardsView handOfCardsView = new HandOfCardsView(handOfCardsController);
    HandOfCardsStatsView handOfCardsStatsView = new HandOfCardsStatsView(handOfCardsController);
    HandOfCardsButtonView handOfCardsButtonView = new HandOfCardsButtonView(handOfCardsController);

    handOfCardsController.addSubscriber(handOfCardsView);
    handOfCardsController.addSubscriber(handOfCardsStatsView);

    BorderPane borderPane = new BorderPane();

    borderPane.setBackground(Background.fill(Color.GREEN));

    borderPane.setCenter(handOfCardsView);
    borderPane.setBottom(handOfCardsStatsView);
    borderPane.setRight(handOfCardsButtonView);

    return borderPane;
  }
}
