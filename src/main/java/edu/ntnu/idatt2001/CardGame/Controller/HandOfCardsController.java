package edu.ntnu.idatt2001.CardGame.Controller;

import edu.ntnu.idatt2001.CardGame.Model.HandOfCards;
import edu.ntnu.idatt2001.CardGame.Listener.HandOfCardsChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that controls hands of cards and notifies listeners whenever there is a change.
 *
 * @author vemundroe
 */
public class HandOfCardsController {
  private List<HandOfCardsChangeListener> listeners;
  private HandOfCards handOfCards;

  /**
   * Constructor for hands of cards controller.
   *
   * @param handOfCards hand of cards
   */
  public HandOfCardsController(HandOfCards handOfCards) {
    this.handOfCards = handOfCards;
    this.listeners = new ArrayList<>();
  }

  /**
   * Method to deal hands and notify listeners.
   */
  public void dealHands() {
    handOfCards.dealHands(5);
    listeners.forEach(listener -> listener.updateHandOfCards(handOfCards));
  }

  /**
   * Method to check hands and notify listeners.
   */
  public void checkHands() {
    listeners.forEach(listener -> listener.updateStats(handOfCards));
  }

  /**
   * Method to get the hand of cards.
   *
   * @return a list of {@code PlayingCard}
   */
  public HandOfCards getHandOfCards() {
    return handOfCards;
  }

  /**
   * Method for a listener to subscribe to changes.
   *
   * @param listener the listener
   */
  public void addSubscriber(HandOfCardsChangeListener listener) {
    if (listener == null) {
      throw new NullPointerException("Listener can not be null.");
    }
    listeners.add(listener);
  }

  /**
   * Method for a listener to unsubscribe to changes.
   *
   * @param listener the listener
   */
  public void removeSubscriber(HandOfCardsChangeListener listener) {
    if (listener == null) {
      throw new NullPointerException("Listener can not be null.");
    }
    listeners.remove(listener);
  }
}
