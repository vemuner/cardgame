package edu.ntnu.idatt2001.CardGame.Listener;

import edu.ntnu.idatt2001.CardGame.Model.HandOfCards;

/**
 * Listener interface with an update method.
 *
 * @author vemundroe
 */
public interface HandOfCardsChangeListener {
  /**
   * Method to update the listener with the new hand of cards.
   *
   * @param handOfCards hand of cards
   */
  void updateHandOfCards(HandOfCards handOfCards);

  /**
   * Method to update the listener about the new stats.
   *
   * @param handOfCards hand of cards
   */
  void updateStats(HandOfCards handOfCards);
}
