package edu.ntnu.idatt2001.CardGame;

/**
 * Class responsible for launching application.
 *
 * @author vemundroe
 */
public class Main {
  /**
   * Launches CardGameApplication
   *
   * @param args args
   */
  public static void main(String[] args) {
    CardGameApplication.main(args);
  }
}
