package edu.ntnu.idatt2001.CardGame.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;

/**
 * Class representing a deck of cards.
 *
 * @author vemundroe
 */
public class DeckOfCards {
  private static Random random = new Random();
  private final char[] suits = {'S', 'H', 'D', 'C'};

  private HashMap<String, PlayingCard> playingCards = new HashMap<>();

  /**
   * Constructor to instantiate a new deck of cards with 52.
   */
  public DeckOfCards() {
    for (int i = 1; i <= 13; i++) {
      for (char suit: suits) {
        playingCards.put(suit+String.valueOf(i), new PlayingCard(suit, i));
      }
    }
  }

  /**
   * Method to deal hands from the deck of cards.
   *
   * @param numberOfCards number of cards to deal out
   * @return a collection with {@code PlayingCard}
   */
  public Collection<PlayingCard> dealHands(int numberOfCards) {
    if (numberOfCards < 5 || numberOfCards > 52) {
      throw new IllegalArgumentException("You must pick between 5 and 52 cards");
    }
    Collection<PlayingCard> randomCards = new ArrayList<>();

    for (int i = 0; i < numberOfCards; i++) {
      randomCards.add(new PlayingCard(suits[random.nextInt(0, 4)], random.nextInt(1, 13)));
    }

    return randomCards;
  }

  /**
   * Method to get the playing cards.
   *
   * @return HashMap with {@code PlayingCard}
   */
  public HashMap<String, PlayingCard> getPlayingCards() {
    return playingCards;
  }
}
