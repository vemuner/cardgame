package edu.ntnu.idatt2001.CardGame.Model;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class representing a hand of cards.
 *
 * @author vemundroe
 */
public class HandOfCards {
  private Collection<PlayingCard> playingCards;

  /**
   * Constructor for instantiating a new hand of cards.
   *
   * @param numberOfCards the number of cards to deal out
   */
  public HandOfCards(int numberOfCards) {
    DeckOfCards deckOfCards = new DeckOfCards();
    playingCards = deckOfCards.dealHands(numberOfCards);
  }

  /**
   * Method to get the playing cards.
   *
   * @return collection of {@code PlayingCard}
   */
  public Collection<PlayingCard> getPlayingCards() {
    return playingCards;
  }

  /**
   * Method to deal a new hand of cards.
   *
   * @param numberOfCards the number of cards to deal
   */
  public void dealHands(int numberOfCards) {
    DeckOfCards deckOfCards = new DeckOfCards();
    playingCards = deckOfCards.dealHands(numberOfCards);
  }

  /**
   * Method to get the total sum of the cards.
   *
   * @return the total sum of the cards
   */
  public int sumOfCards() {
    return playingCards.stream().mapToInt(PlayingCard::getFace).sum();
  }

  /**
   * Method to retrieve all the cards in the hand of suit 'Heart'.
   *
   * @return a list of {@code PlayingCard}
   */
  public Collection<PlayingCard> getHearts() {
    return playingCards.stream().filter(playingCard -> playingCard.getSuit() == 'H').toList();
  }

  /**
   * Method to calculate if a hand has the queen of spades.
   *
   * @return {@code true} if player has the queen of spades, else {@code false}
   */
  public boolean hasQueenOfSpades() {
    return playingCards.stream().filter(playingCard -> playingCard.getAsString().equals("S12")).findFirst().isPresent();
  }

  /**
   * Method to calculate if a hand has a five flush.
   *
   * @return {@code true} if player has five cards with same suit, else {@code false}
   */
  public boolean isFiveFlush() {
    return playingCards.stream()
      .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()))
      .values().stream().anyMatch(count -> count >= 5);
  }
}
