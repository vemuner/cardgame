package edu.ntnu.idatt2001.CardGame.View;

import edu.ntnu.idatt2001.CardGame.Controller.HandOfCardsController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * Class showing the buttons 'Deal hands' and 'Check hands'.
 *
 * @author vemundroe
 */
public class HandOfCardsButtonView extends VBox {
  private HandOfCardsController handOfCardsController;

  /**
   * Constructor to instantiate the buttons.
   *
   * @param handOfCardsController hand of cards controller
   */
  public HandOfCardsButtonView(HandOfCardsController handOfCardsController) {
    this.handOfCardsController = handOfCardsController;

    createAndConfigureButtons();
  }

  /**
   * Method to create and configure the buttons.
   */
  private void createAndConfigureButtons() {
    this.setAlignment(Pos.CENTER);
    this.setSpacing(10);
    this.setPadding(new Insets(10));
    this.getChildren().add(dealHandsButton());
    this.getChildren().add(checkHandsButton());
  }

  /**
   * Method to set up the 'deal hands' button.
   *
   * @return the 'deal hands' button
   */
  private Button dealHandsButton() {
    Button dealHandsButton = new Button("Deal Hands");
    dealHandsButton.setPrefWidth(100);
    dealHandsButton.setPrefHeight(40);
    dealHandsButton.setOnAction(actionEvent -> {
      handOfCardsController.dealHands();
    });
    return dealHandsButton;
  }

  /**
   * Method to set up the 'check hands' button.
   *
   * @return the 'check hands' button
   */
  private Button checkHandsButton() {
    Button checkHandsButton = new Button("Check Hands");
    checkHandsButton.setPrefWidth(100);
    checkHandsButton.setPrefHeight(40);
    checkHandsButton.setOnAction(actionEvent -> {
      handOfCardsController.checkHands();
    });
    return checkHandsButton;
  }
}
