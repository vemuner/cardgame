package edu.ntnu.idatt2001.CardGame.View;

import edu.ntnu.idatt2001.CardGame.Controller.HandOfCardsController;
import edu.ntnu.idatt2001.CardGame.Model.HandOfCards;
import edu.ntnu.idatt2001.CardGame.Listener.HandOfCardsChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Class showing the stats of the hand of cards.
 *
 * @author vemundroe
 */
public class HandOfCardsStatsView extends HBox implements HandOfCardsChangeListener {
  Text sumOfFacesText;
  Text cardsOfHeartsText;
  Text isFiveFlushText;
  Text hasQueenOfSpadesText;

  /**
   * Constructor to instantiate the view for showing stats.
   *
   * @param handOfCardsController hand of cards controller
   */
  public HandOfCardsStatsView(HandOfCardsController handOfCardsController) {

    createAndConfigureStats(handOfCardsController.getHandOfCards());
  }

  /**
   * Method to create and configure the stats.
   *
   * @param handOfCards hand of cards
   */
  private void createAndConfigureStats(HandOfCards handOfCards) {
    this.setSpacing(10);
    this.setAlignment(Pos.CENTER);
    this.setPadding(new Insets(10, 10, 20, 10));
    sumOfFacesText = new Text("Sum of faces: " + handOfCards.sumOfCards());
    sumOfFacesText.setFill(Color.WHITE);
    this.getChildren().add(sumOfFacesText);

    StringBuilder cardsOfHeartsStringBuilder = new StringBuilder();
    handOfCards.getHearts().forEach(playingCard -> cardsOfHeartsStringBuilder.append(playingCard.getAsString()).append(" "));
    cardsOfHeartsText = new Text("Cards of hearts: " + cardsOfHeartsStringBuilder);
    cardsOfHeartsText.setFill(Color.WHITE);
    this.getChildren().add(cardsOfHeartsText);

    isFiveFlushText = new Text("Is flush: " + handOfCards.isFiveFlush());
    isFiveFlushText.setFill(Color.WHITE);
    this.getChildren().add(isFiveFlushText);

    hasQueenOfSpadesText = new Text("Has Queen of spades: " + handOfCards.hasQueenOfSpades());
    hasQueenOfSpadesText.setFill(Color.WHITE);
    this.getChildren().add(hasQueenOfSpadesText);
  }

  @Override
  public void updateStats(HandOfCards handOfCards) {
    sumOfFacesText.setText("Sum of faces: " + handOfCards.sumOfCards());

    StringBuilder cardsOfHeartsStringBuilder = new StringBuilder();
    handOfCards.getHearts().forEach(playingCard -> cardsOfHeartsStringBuilder.append(playingCard.getAsString()).append(" "));
    cardsOfHeartsText.setText("Cards of hearts: " + cardsOfHeartsStringBuilder);

    isFiveFlushText.setText("Is flush: " + handOfCards.isFiveFlush());

    hasQueenOfSpadesText.setText("Has Queen of spades: " + handOfCards.hasQueenOfSpades());
  }

  @Override
  public void updateHandOfCards(HandOfCards handOfCards) {}
}
