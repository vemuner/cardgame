package edu.ntnu.idatt2001.CardGame.View;

import edu.ntnu.idatt2001.CardGame.Controller.HandOfCardsController;
import edu.ntnu.idatt2001.CardGame.Model.HandOfCards;
import edu.ntnu.idatt2001.CardGame.Model.PlayingCard;
import edu.ntnu.idatt2001.CardGame.Listener.HandOfCardsChangeListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * Class showing the hand of cards.
 *
 * @author vemundroe
 */
public class HandOfCardsView extends HBox implements HandOfCardsChangeListener {
  private Collection<ImageView> cardImages = new ArrayList<>();
  private final HandOfCardsController handOfCardsController;

  /**
   * Constructor instantiate the view showing the cards.
   *
   * @param handOfCardsController hand of cards controller
   */
  public HandOfCardsView(HandOfCardsController handOfCardsController) {
    this.handOfCardsController = handOfCardsController;

    try {
      configureView();
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Method to configure the view.
   *
   * @throws FileNotFoundException file not found exception
   */
  private void configureView() throws FileNotFoundException {
    this.setSpacing(10);
    this.setAlignment(Pos.CENTER);

    updateView(handOfCardsController.getHandOfCards().getPlayingCards());
  }

  /**
   * Method to update the view.
   *
   * @param playingCards list of playing cards to display
   * @throws FileNotFoundException file not found exception
   */
  private void updateView(Collection<PlayingCard> playingCards) throws FileNotFoundException {
    this.getChildren().clear();
    cardImages.clear();

    for (PlayingCard playingCard : playingCards) {
      Image image = new Image(new FileInputStream("src/main/resources/images/" + playingCard.getAsString() + ".png"));
      ImageView imageView = new ImageView(image);
      imageView.setFitWidth(100);
      imageView.setPreserveRatio(true);
      cardImages.add(imageView);
    }

    this.getChildren().addAll(cardImages);
  }

  @Override
  public void updateHandOfCards(HandOfCards handOfCards) {
    try {
      updateView(handOfCards.getPlayingCards());
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  @Override
  public void updateStats(HandOfCards handOfCards) {}
}
