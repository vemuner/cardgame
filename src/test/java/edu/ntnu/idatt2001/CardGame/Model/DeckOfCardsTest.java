package edu.ntnu.idatt2001.CardGame.Model;

import java.util.Collection;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeckOfCardsTest {
  @Test
  public void instantiate_a_deck_of_cards() {
    DeckOfCards deckOfCards = new DeckOfCards();

    assertEquals(52, deckOfCards.getPlayingCards().size());
  }

  @Test
  public void get_random_card() {
    DeckOfCards deckOfCards = new DeckOfCards();
    int numberOfCards = 5;

    Collection<PlayingCard> playingCards = deckOfCards.dealHands(numberOfCards);

    assertEquals(numberOfCards, playingCards.size());
  }
}
