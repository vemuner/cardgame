package edu.ntnu.idatt2001.CardGame.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

public class HandOfCardsTest {
  @Test
  public void sum_of_cards() {
    HandOfCards handOfCards = new HandOfCards(5);
    int expectedSum = 0;

    for (PlayingCard playingCard : handOfCards.getPlayingCards()) {
      expectedSum += playingCard.getFace();
    }

    assertEquals(expectedSum, handOfCards.sumOfCards());
  }

  @Test
  public void get_hearts() {
    HandOfCards handOfCards = new HandOfCards(5);

    assertTrue(handOfCards.getHearts().stream().filter(playingCard -> playingCard.getSuit() != 'H').toList()
      .size() == 0);
  }

  @Test
  public void has_queen_of_spades() {
    HandOfCards handOfCards = new HandOfCards(5);

    while (!handOfCards.hasQueenOfSpades()) {
      handOfCards = new HandOfCards(5);
    }

    assertTrue(handOfCards.hasQueenOfSpades());
  }

  @Test
  public void is_five_flush() {
    HandOfCards handOfCards = new HandOfCards(5);

    while (!handOfCards.isFiveFlush()) {
      handOfCards = new HandOfCards(5);
    }

    assertTrue(handOfCards.isFiveFlush());
  }
}
