package edu.ntnu.idatt2001.CardGame.Model;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

public class PlayingCardTest {
  @Test
  public void instantiate_playing_card_correctly() {
    try {
      new PlayingCard('S', 13);
    } catch (Exception exception) {
      fail();
    }
  }

  @Test
  public void instantiate_playing_card_incorrectly() {
    try {
      new PlayingCard('K', 13);
      fail();
    } catch (Exception exception) {
      System.out.println(exception.getMessage());
      if (!(exception.getMessage().equals("Suit must be either 'S', 'H', 'D' or 'C'") || !exception.getMessage().equals("Face must be between 1 and 13"))) {
        fail();
      }
    }
  }
}
